#include "DynamixelMotor.h"
#define Dynamixel1ID 2
#define Dynamixel2ID 14
#define Dynamixel3ID 10
#define Dynamixel4ID 4

// id of the motor
const uint8_t id1=2;
const uint8_t id2=14;
const uint8_t id3=10;
const uint8_t id4=4;
// speed, between -1023 and 1023
int16_t speed=50;
// communication baudrate
const long unsigned int baudrate = 1000000;
char place;
boolean newData = false;

// hardware serial without tristate buffer
// see blink_led example, and adapt to your configuration
HardwareDynamixelInterface interface(Serial);

DynamixelMotor motor1(interface, id1);
DynamixelMotor motor2(interface, id2);
DynamixelMotor motor3(interface, id3);
DynamixelMotor motor4(interface, id4);

void get_place() {
 if (Serial.available() > 0) {
 place = Serial.read();
 newData = true;
 }
}

void nr_inspection(){
 if (newData == true) {
  
  if(place == '1'){
   Serial.println("Inspection");
   motor1.speed(365);
   motor2.speed(-400);
   delay(11000);
   motor1.speed(0);
   motor2.speed(0);
  }

  if(place == '2'){
   Serial.println("Finished Inspection one side");
   motor1.speed(365);
   motor2.speed(-400);
   delay(1000);
   motor1.speed(0);
   motor2.speed(0);
   delay(1000);
   Serial.println("Call Gripper");
   motor3.goalPosition(531);
   delay(3000);
   Serial.println("Gripper position");
   motor1.speed(-365);
   motor2.speed(400);
   delay(1000);
   motor1.speed(0);
   motor2.speed(0);
   delay(1000);
   Serial.println("Grip Tool");
   digitalWrite(5,LOW);      // added 9 Dec 2021 by Burhan, start
   digitalWrite(6,LOW);      // pin 7 is ENABLE. HIGH is to OFF. LOW to ON
   digitalWrite(7,LOW);       // pin 5 and 6 need to move together to determine the direction.
   delay(1000);
   digitalWrite(7,HIGH);
   delay(1000);               // end
   //digitalWrite(7,HIGH);
   //delay(2000);
   //digitalWrite(7,LOW);
   //delay(2000);
  }

  if(place == '3'){
   Serial.println("Flip Gripper");
   motor3.goalPosition(795);
   delay(4000);
   
  }

  if(place == '4'){
   Serial.println("UnGrip Tool");
   digitalWrite(5,HIGH);       // added 9 Dec 2021 by Burhan, start
   digitalWrite(6,HIGH);       // pin 7 is ENABLE. HIGH is to OFF. LOW to ON
   digitalWrite(7,LOW);       // pin 5 and 6 need to move together to determine the direction.
   delay(2000);               
   digitalWrite(7,HIGH);
   delay(1000);               // end
   //digitalWrite(5,HIGH);
   //digitalWrite(6,HIGH);
   //delay(2000);
   //digitalWrite(5,LOW);
   //digitalWrite(6,LOW);
   //delay(1000);
  }
  
  if(place == '5'){
   Serial.println("Tool Initial Place");
   motor1.speed(-365);
   motor2.speed(400);
   delay(1000);
   motor1.speed(0);
   motor2.speed(0);
  }

  if(place == '6'){
   Serial.println("Slide Gripper");
   motor3.goalPosition(835);
   delay(1000);
  }

  if(place == '7'){
   Serial.println("Inspection Done");
   motor1.speed(365);
   motor2.speed(-400);
   delay(5000);
   motor1.speed(0);
   motor2.speed(0); 
  }
  if(place == '8'){
    Serial.println("Widening Conveyor");
    motor4.goalPosition(510);
  }
  if(place == '9'){
    Serial.println("Narrowing Conveyor");
    motor4.goalPosition(630);
  }
  newData = false;
  place = '0';
 }
}
void setup()
{ 
  interface.begin(baudrate);
  delay(1000);
  motor1.enableTorque(); 
  motor2.enableTorque();
  motor3.enableTorque();
  motor4.enableTorque(); 
  motor1.wheelMode(); //conveyor motor 1
  motor2.wheelMode(); //conveyor motor2
  motor3.jointMode(204,835); //gripper movement motor
  motor4.jointMode(350,630); //base motor
  //gripping motor
  pinMode(7,OUTPUT);
  pinMode(6,OUTPUT);
  pinMode(5,OUTPUT);
  //defining speed for joint motors
  digitalWrite(7,HIGH);     // initialize the relay. HIGH is to OFF the relay. Burhan 9 December 2021
  motor3.speed(speed);
  motor4.speed(speed);
  
  
}

//change motion direction every 5 seconds
void loop() 
{
  get_place();
  nr_inspection();
  
}
