/*
 * Example showing how to send position commands to AX-12A
 */

#include "Arduino.h"
#include "AX12A.h"

#define DirectionPin 12
#define BaudRate 1000000
//#define ID 13 // first drawer bottom row
//#define ID 17 // middle drawer bottom row
//#define ID 7 // third drawer bottom row
//#define ID 20 // first drawer top row
//#define ID 26 // middle drawer top row
//#define ID 5 // third drawer top row
//#define ID 9// sample 
//#define ID 14
#define DELAY 1000  //Burhan 14 12 21

int a = 0;      //real motor ID Burhan 14 12 21
char b;      //selection of the drawer Burhan 14 12 21

int openingPos = 0;
int closingPos = 420; //was 379, 406

int openPin = 5;
int closePin = 4;
int mode = 0; //0 means motor available, 1 means opening, 2 means closing

int initial_pos = 512;

int pos = initial_pos;


void resetServo(){
  Serial.println("getting servo to opening position...");
  pos = openingPos;
  ax12a.move(a, pos);
  delay(DELAY);
}


void activateServo(){
  if(mode == 1) pos = openingPos; 
  else pos = closingPos;
  ax12a.move(a, pos);
  delay(DELAY);   //Burhan 14 12 21
  Serial.println();
  Serial.println(pos);
  Serial.println(a);
  return true;
}

void setup()
{
  Serial.begin(BaudRate);   // Open serial port for reading input from PC Burhan 14 12 21
  ax12a.begin(BaudRate, DirectionPin, &Serial);
  pinMode(openPin, INPUT);
  pinMode(closePin, INPUT);
//  resetServo();
}

void loop()
{
  if (Serial.available() > 0) {       
    // read the incoming byte:
    b = Serial.read();      // reading input from PC Burhan 14 12 21
    Serial.println(b);
    //for(a=1; a<10;a++){
    //  Serial.println(a);

    switch (b) {          //conversion from options to motor ID //Burhan 14 12 21
      case '1':
      a = 20;  // statements
      break;
      case '2':
      a = 26 ; // statements
      break;
      case '3':
      a = 5  ; // statements
      break;
      case '4':
      a = 13 ; // statements
      break;
      case '5':
      a = 17 ; // statements
      break;
      case '6':
      a = 7  ; // statements;
      break;
      default:
      a = 254 ;// statements
      Serial.println("wrong ID selection. Only 1 to 6 are valid");
      break;                //Burhan 14 12 21
    }

   // if((digitalRead(openPin) == LOW) && (mode == 0)){           // remove this 
      Serial.println("Cover dropping, deactivating cover holder...");
      mode = 1;
      activateServo();
      mode = 0;
      Serial.println("Operation completed successfully");
      Serial.println();
   // }
 
   // if((digitalRead(closePin) == LOW) && (mode == 0)){
      Serial.println("Activating cover holder...");
      mode = 2;
      activateServo();
      mode = 0;
      Serial.println("Operation completed successfully");
      Serial.println();
    //}
  }

}
