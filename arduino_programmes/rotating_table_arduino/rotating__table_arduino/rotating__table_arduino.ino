
#include <Servo.h>

bool completed = false;

const int RbuttonPin = 7;// processing table sin
const int FbuttonPin = 8;// flip sin
const int FRbuttonPin = 2;// reverse flip sin


const int left= 5;  
const int right = 6; 
int t = 0;


Servo servoa; 
Servo servob; 

int FbuttonState = 0;
int FRbuttonState = 0;
int RbuttonState = 0;

void setup() {
 servoa.attach(3); 
 servob.attach(11);
 
 pinMode(FbuttonPin, INPUT);
 pinMode(RbuttonPin, INPUT);
 pinMode(FRbuttonPin, INPUT);
 pinMode(left, OUTPUT);  
 pinMode(right, OUTPUT);
 
 digitalWrite(left,LOW);
 digitalWrite(right,LOW);
 
 servoa.write(0);
 servob.write(0);
  
}

void loop() {
  // put your main code here, to run repeatedly:
 FbuttonState = digitalRead(FbuttonPin);
 FRbuttonState = digitalRead(FRbuttonPin);
 RbuttonState = digitalRead(RbuttonPin);
  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (FbuttonState == HIGH) 
  {
      servoa.write(180);
      servob.write(5);

     delay(1000);
      servoa.write(5);
      servob.write(180);  
      delay(1000);  
  } 
  else
  {
      servoa.write(0);
      servob.write(0);
  }


if (FRbuttonState == HIGH) 
  {
      servoa.write(5);
      servob.write(180);

     delay(1000);
      servoa.write(180);
      servob.write(5);  
      delay(1000);  
  } 
  else
  {
      servoa.write(0);
      servob.write(0);
  }



  if (RbuttonState == HIGH && !completed) 
  {
       digitalWrite(left,LOW);
       digitalWrite(right,HIGH);
       delay(100);
       digitalWrite(left,LOW);
       digitalWrite(right,LOW);
       delay(100);
       
     for ( t = 0; t <= 3; t += 1) 
     { 
       digitalWrite(right,LOW);
       digitalWrite(left,HIGH);
       delay(250);
       digitalWrite(left,LOW);
       digitalWrite(right,LOW);
       delay(100);
       digitalWrite(left,LOW);
       digitalWrite(right,HIGH);
       delay(250); 
       digitalWrite(left,LOW);
       digitalWrite(right,LOW);
       delay(100);     
     }
     

     
     digitalWrite(right,LOW);
     digitalWrite(left,LOW);
     completed = true;
  } 
  if (RbuttonState == LOW){
    
     completed = false;
  }
  else{
//    digitalWrite(left,LOW);
//     digitalWrite(right,LOW);
     }
  
  }




void current_motor_spinner(){
  if (RbuttonState == HIGH && !completed) 
  {
       digitalWrite(left,LOW);
       digitalWrite(right,HIGH);
       delay(50);
       digitalWrite(left,LOW);
       digitalWrite(right,LOW);
       delay(150);
       
     for ( t = 0; t <= 3; t += 1) 
     { 
       digitalWrite(right,LOW);
       digitalWrite(left,HIGH);
       delay(300);
       digitalWrite(left,LOW);
       digitalWrite(right,LOW);
       delay(100);
       digitalWrite(left,LOW);
       digitalWrite(right,HIGH);
       delay(300); 
       digitalWrite(left,LOW);
       digitalWrite(right,LOW);
       delay(100);     
     }
     

     
     digitalWrite(right,LOW);
     digitalWrite(left,LOW);
     completed = true;
  } 
}
