#!/usr/bin/env python
import socket, sys
import yaml
from box import Box
import rospy
import rospkg
from plc_server.msg import PLCMsg
from plc_server.srv import *
import enum


HOST = '192.168.10.103'
PORT = 5050
pin_dictionary = None
slmp_write = "500000FF03FF00001800001402000101M*"
# slmp_read_sample = "500000FF03FF000018000004010001X*0000000010*"
slmp_read = "500000FF03FF000018000004010001"

rospack = rospkg.RosPack()
rospy.init_node('plc_server')
r = rospy.Rate(10) # 10hz

def load_yaml():
    global pin_dictionary
    dir = rospack.get_path('plc_server') + '/config/pin_dictionary.yaml'
    with open(dir, 'r') as file:
        pin_dictionary = Box(yaml.load(file, Loader=yaml.FullLoader))

def reset_and_restart_plc():
    #debug system reset
    plc.write(0, '00')
    plc.write(7, '01')
    plc.write(7, '00')

    #system start
    plc.write(0, '01')
    return True

class preset_value(enum.Enum):
    HIGH = "01"
    LOW = "00"

class PLCServiceServer(object):
    def __init__(self):
        self.service = rospy.Service('plc_server', PLCService, self.execute)
    
    def execute(self, req):
        print("service called")
        response = PLCServiceResponse()
        if req.task == req.WRITE:
            try:
                pin = pin_dictionary[req.defined_pin]
                if req.value:
                    response.success = plc.write(pin, preset_value.HIGH.value)
                else:
                    response.success = plc.write(pin, preset_value.LOW.value)
            except:
                print("cant find pin")
                response.success = False

        elif req.task == req.READ:
            read_status = plc.read(req.custom_pin) 
            if read_status[0]:
                response.return_value = int(read_status[1])
                response.success = True

        elif req.task == req.READ_BULK:
            """bulk read a series of pin from plc"""
            module = req.bulk_read_module

            if module == "wetbay_drybay_input":
                #bulk read wetbay tray presense and drybay cover and container presense
                wetbay_return_list = []
                drybay_return_list = []
                try:
                    for i in range(1,7):
                        # print(pin_dictionary["WETBAY_TRAY_PRESENSE_" + str(i)])
                        tray_pin = pin_dictionary["WETBAY_TRAY_PRESENSE_" + str(i)]
                        cover_pin = pin_dictionary["DRYBAY_COVER_PRESENSE_" + str(i)]
                        container_pin = pin_dictionary["DRYBAY_CONTAINER_PRESENSE_" + str(i)]

                        plc_result_tray = plc.read(tray_pin)
                        plc_result_cover = plc.read(cover_pin)
                        plc_result_container = plc.read(container_pin)

                        if plc_result_tray[0] and plc_result_cover[0] and plc_result_container[0]:
                            plc_result_tray = int(plc_result_tray[1])
                            wetbay_return_list.append(plc_result_tray)

                            cover_container_result = int(str(plc_result_cover[1]) + str(plc_result_container[1]))
                            drybay_return_list.append(cover_container_result)
                        else:
                            raise Exception("plc read unsuccessful")

                    response.bulk_read_return_value = wetbay_return_list + drybay_return_list
                    response.success = True
                except:
                    response.success = False


        elif req.task == req.WRITE_CUSTOM:
            if req.value:
                response.success = plc.write_custom(req.relay_type, req.custom_pin, preset_value.HIGH.value)
            else:
                response.success = plc.write_custom(req.relay_type, req.custom_pin, preset_value.LOW.value)
        
        elif req.task == req.RESET:
            reset_and_restart_plc()
            
        print(response)
        return response


class plc_server(object):
    def __init__(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            print("connecting to plc") 
            self.sock.connect((HOST, PORT))
            print("successfully connected")
        except: 
            print("connection failed, is plc turned on?")
            sys.exit()
    
    def write(self, pin, state):
        data = slmp_write + str(pin).zfill(6) + state
        print(data)
        self.sock.send(data)
        receive = self.sock.recv(1024)
        if receive[18:22] == '0000':
            # no error
            print("write successful")
            return True
        else: 
            return False

    def write_custom(self, relay_type, pin, state):
        data = slmp_write[:-2] + relay_type + "*" + str(pin).zfill(6) + state
        print(data)
        self.sock.send(data)
        receive = self.sock.recv(1024)
        if receive[18:22] == '0000':
            # no error
            print("write successful")
            return True
        else: 
            return False

    def read(self, pin):
        input_io = []
        read_output = [False, 0]
        data = slmp_read + pin[0] + "*" + pin[1:].zfill(6) + "0001"
        self.sock.send(data)
        self.sock.settimeout(2.0)
        try:
            receive = self.sock.recv(1024)
            if receive[18:22] == '0000':
                print("read successful")
                input_io = receive[22:]
                read_output[1] = int(input_io)
                read_output[0] = True
            else: 
                print("reading unsuccessful")
                read_output[0] = False
        except:
            print("error while receiving data from plc")
            read_output[0] = False
        return read_output

plc = plc_server() 
server = PLCServiceServer()       
load_yaml()

reset_and_restart_plc()

while not rospy.is_shutdown():
    r.sleep()