#!/usr/bin/env python
import socket, sys
from pexpect import which
import yaml
from box import Box
import rospy
import rospkg
from plc_server.msg import PLCMsg
from plc_server.srv import *
import enum
from time import sleep
import os

MASTER = True
HOST = '192.168.10.103'
PORT = 5050
pin_dictionary = None
slmp_write = "500000FF03FF00001800001402000101M*"
# slmp_read_sample = "500000FF03FF000018000004010001X*0000000010*"
slmp_read = "500000FF03FF000018000004010001"

UR10_IP = "192.168.10.101"
UR5_IP = "192.168.10.196"

rospack = rospkg.RosPack()
rospy.init_node('plc_server')
r = rospy.Rate(10) # 10hz


SOCK = None

def load_yaml():
    global pin_dictionary
    dir = rospack.get_path('plc_server') + '/config/pin_dictionary.yaml'
    with open(dir, 'r') as file:
        pin_dictionary = Box(yaml.load(file, Loader=yaml.FullLoader))

def reset_and_restart_plc():
    #debug system reset
    success_counter = 0
    success_counter += plc.write(0, '00')
    success_counter += plc.write(7, '01')
    success_counter += plc.write(7, '00')

    #system start
    success_counter += plc.write(0, '01')
    if success_counter == 4:
        return True
    else:
        return False

def ping_check(ip):
    response = os.system("timeout 0.2 ping -c 1 " + str(ip))
    if response == 0:
        #detected
        return True
    else:
        return False


def bootup_ur(which_ur):
    print("pinging " + which_ur + " ip to check if it is powered on")
    if ping_check(eval(which_ur + "_IP")):
        print(which_ur + " has been booted")
        return True
        
    else:
        print("ping check fail, " + which_ur +" not powered, powering up now")
        goal = PLCServiceRequest()
        goal.task = goal.BOOT_UR
        goal.defined_pin = which_ur + "_BOOTUP"
        if server.execute(goal).success:
            sleep(45)
            if ping_check(eval(which_ur + "_IP")):
                print(which_ur + " has been booted")
                return True
            else:
                print("error botting " + which_ur)
        else:
            return False

def read_wetbay_drybay_pneumatic():
    '''read and return an array of 12 representing the wetbay and drybay pneumatic status or False on error'''
    wetbay_pneumatic, drybay_pneumatic = [], []
    try:
        for i in range(1,7):
            wetbay_drawer_pin = 'M' + str(pin_dictionary["WETBAY_DRAWER_" + str(i)])
            wetbay_recovery_pin = 'M' + str(pin_dictionary["WETBAY_RECOVERY_" + str(i)])
            drybay_drawer_pin = 'M' + str(pin_dictionary["DRYBAY_DRAWER_" + str(i)])

            plc_result_wetbay_drawer_pin = plc.read(wetbay_drawer_pin)
            plc_result_wetbay_recovery_pin = plc.read(wetbay_recovery_pin)
            plc_result_drybay = plc.read(drybay_drawer_pin)

            if plc_result_wetbay_drawer_pin[0] and plc_result_wetbay_recovery_pin[0] and plc_result_drybay[0]:
                plc_result_wetbay = int(plc_result_wetbay_drawer_pin[1]) or int(plc_result_wetbay_recovery_pin[1])
                wetbay_pneumatic.append(plc_result_wetbay)

                drybay_pneumatic.append(plc_result_drybay[1])
            else:
                return False

        bulk_read_return_value = wetbay_pneumatic + drybay_pneumatic
        return bulk_read_return_value
    except:
        return False

class preset_value(enum.Enum):
    HIGH = "01"
    LOW = "00"

class PLCServiceServer(object):
    def __init__(self):
        self.service = rospy.Service('plc_server', PLCService, self.execute)
    
    def execute(self, req):
        global SOCK
        print("service called")
        response = PLCServiceResponse()
        SOCK = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            SOCK.connect((HOST, PORT))
        except: 
            print("connection failed, is plc turned on?")
            response.success = False
            return response

        if req.task == req.WRITE:
            try:
                pin = pin_dictionary[req.defined_pin]
                if req.value:
                    response.success = plc.write(pin, preset_value.HIGH.value)
                else:
                    response.success = plc.write(pin, preset_value.LOW.value)
            except:
                print("cant find pin")
                response.success = False

        elif req.task == req.READ:
            read_status = plc.read(req.custom_pin) 
            if read_status[0]:
                response.return_value = int(read_status[1])
                response.success = True

        elif req.task == req.READ_BULK:
            """bulk read a series of pin from plc"""
            module = req.bulk_read_module

            if module == "wetbay_drybay_input":
                #bulk read wetbay tray presense and drybay cover and container presense
                wetbay_return_list = []
                drybay_return_list = []
                try:
                    for i in range(1,7):
                        # print(pin_dictionary["WETBAY_TRAY_PRESENSE_" + str(i)])
                        tray_pin = pin_dictionary["WETBAY_TRAY_PRESENSE_" + str(i)]
                        cover_pin = pin_dictionary["DRYBAY_COVER_PRESENSE_" + str(i)]
                        container_pin = pin_dictionary["DRYBAY_CONTAINER_PRESENSE_" + str(i)]

                        plc_result_tray = plc.read(tray_pin)
                        plc_result_cover = plc.read(cover_pin)
                        plc_result_container = plc.read(container_pin)

                        if plc_result_tray[0] and plc_result_cover[0] and plc_result_container[0]:
                            plc_result_tray = int(plc_result_tray[1])
                            wetbay_return_list.append(plc_result_tray)

                            cover_container_result = int(str(plc_result_cover[1]) + str(plc_result_container[1]))
                            drybay_return_list.append(cover_container_result)
                        else:
                            raise Exception("plc read unsuccessful")

                    response.bulk_read_return_value = wetbay_return_list + drybay_return_list
                    response.success = True
                except:
                    response.success = False

            elif module == "wetbay_drybay_pneumatic":
                bulk_read_return_value = read_wetbay_drybay_pneumatic()
                if bulk_read_return_value != False:
                    response.bulk_read_return_value = bulk_read_return_value
                    response.success = True
                else:
                    response.success = False

        elif req.task == req.WRITE_CUSTOM:
            if req.value:
                response.success = plc.write_custom(req.relay_type, req.custom_pin, preset_value.HIGH.value)
            else:
                response.success = plc.write_custom(req.relay_type, req.custom_pin, preset_value.LOW.value)
        
        elif req.task == req.WRITE_FROM_UI:
            try:
                current_pneumatic_val = read_wetbay_drybay_pneumatic()
                print(current_pneumatic_val)
                if current_pneumatic_val != False:
                    if req.custom_pin == 0 and not current_pneumatic_val[0] and current_pneumatic_val[8]:
                        print("drybay drawer 3 is extended, extending wetbay drawer 1 is not allowed")
                        raise Exception("drybay drawer 3 is extended, extending wetbay drawer 1 is not allowed")
                    elif req.custom_pin == 8 and not current_pneumatic_val[8] and current_pneumatic_val[0]:
                        print("wetbay drawer 1 is extended, extending drybay drawer 3 is not allowed")
                        raise Exception("wetbay drawer 1 is extended, extending drybay drawer 3 is not allowed")
                    if req.custom_pin == 3 and not current_pneumatic_val[3] and current_pneumatic_val[11]:
                        print("drybay drawer 6 is extended, extending wetbay drawer 4 is not allowed")
                        raise Exception("drybay drawer 6 is extended, extending wetbay drawer 4 is not allowed")
                    elif req.custom_pin == 11 and not current_pneumatic_val[11] and current_pneumatic_val[3]:
                        print("wetbay drawer 4 is extended, extending drybay drawer 6 is not allowed")
                        raise Exception("wetbay drawer 4 is extended, extending drybay drawer 6 is not allowed")
                    print("checking successful, writing to pin")
                    if req.custom_pin < 6:
                        pin_name = "WETBAY_RECOVERY_" + str(req.custom_pin + 1)
                    else:
                        pin_name = "DRYBAY_DRAWER_" + str(req.custom_pin -6 + 1)
                    print(pin_name)
                    pin = pin_dictionary[pin_name]
                    print(pin)
                    if current_pneumatic_val[req.custom_pin]:
                        print("writing low")
                        response.success = plc.write(pin, preset_value.LOW.value)
                    else:
                        print("writing high")
                        response.success = plc.write(pin, preset_value.HIGH.value)
                    # sleep(5) #not sure this is needed
                    # current_pneumatic_val = read_wetbay_drybay_pneumatic()
                    # response.bulk_read_return_value = current_pneumatic_val
                else:
                    print("read fail, pneumatic control from ui not allowed at the moment")
                    response.success = False
                    response.message = "read fail, pneumatic control from ui not allowed at the moment"
            except Exception as err:
                response.success = False
                response.message = err.args[0]


        elif req.task == req.BOOT_UR:
            success_counter = 0
            try:
                pin = pin_dictionary[req.defined_pin + ""]
                success_counter += plc.write(pin, preset_value.HIGH.value) 
                sleep(0.5)
                success_counter += plc.write(pin, preset_value.LOW.value)
                if success_counter == 2:
                    response.success = True
                else:
                    print("some thing wrong during ur startup")
                    response.success = False
            except:
                print("cant find pin")
                response.success = False
        
        elif req.task == req.RESET:
            response.success = reset_and_restart_plc()

            #system start
            # plc.write(0, '01')
            
        print(response)
        print("closing sock")
        SOCK.close()
        return response


class plc_server(object):
    
    def write(self, pin, state):
        
        data = slmp_write + str(pin).zfill(6) + state
        print(data)
        SOCK.send(data)
        receive = SOCK.recv(1024)
        if receive[18:22] == '0000':
            # no error
            print("write successful")
            return True
        else: 
            return False

    def write_custom(self, relay_type, pin, state):
        
        data = slmp_write[:-2] + relay_type + "*" + str(pin).zfill(6) + state
        print(data)
        SOCK.send(data)
        receive = SOCK.recv(1024)
        if receive[18:22] == '0000':
            # no error
            print("write successful")
            return True
        else: 
            return False

    def read(self, pin):

        input_io = []
        read_output = [False, 0]
        data = slmp_read + pin[0] + "*" + pin[1:].zfill(6) + "0001"
        SOCK.send(data)
        SOCK.settimeout(2.0)
        try:
            receive = SOCK.recv(1024)
            if receive[18:22] == '0000':
                print("read successful")
                input_io = receive[22:]
                read_output[1] = int(input_io)
                read_output[0] = True
            else: 
                print("reading unsuccessful")
                read_output[0] = False
        except:
            print("error while receiving data from plc")
            read_output[0] = False
        return read_output

plc = plc_server() 
server = PLCServiceServer()       
load_yaml()

if MASTER: 
    goal = PLCServiceRequest()
    goal.task = goal.RESET
    if server.execute(goal).success:
        print("PLC booted successfully")
    else:
        sys.exit(1)

    #power up ur10 and ur5
    # if bootup_ur("UR10") and bootup_ur("UR5"):
    # if bootup_ur("UR10"):
    #     print("UR10 booted successfully")

while not rospy.is_shutdown():
    r.sleep()