#!/usr/bin/env python
from flask import Flask
from flask import request
app = Flask(__name__)


from time import sleep

@app.route('/wrapping_bay', methods = ['GET'])
def wrapping_bay():
    print("wrapping in progress")
    sleep(3)
    return 'True'

@app.route('/non_ring_spare_bay', methods = ['POST'])
def non_ring_spare_bay():
    data = request.get_json()
    print(data)
    return 'True'

if __name__ == '__main__':
    app.run(debug=True, port=5001)